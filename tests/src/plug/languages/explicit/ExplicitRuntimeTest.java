package plug.languages.explicit;

import org.junit.Assert;
import org.junit.Test;
import plug.core.ILanguagePlugin;
import plug.core.IRuntimeView;
import plug.core.IStateSpaceManager;
import plug.core.ITransitionRelation;
import plug.explorer.AbstractExplorer;
import plug.explorer.BFSExplorer;
import plug.language.explicit.ExplicitPlugin;
import plug.language.explicit.diagnosis.AtomEvaluator;
import plug.language.explicit.diagnosis.Predicate;
import plug.language.explicit.runtime.ExplicitConfiguration;
import plug.language.explicit.runtime.ExplicitTransitionRelation;
import plug.statespace.SimpleStateSpaceManager;
import plug.verifiers.deadlock.DeadlockVerifier;
import plug.verifiers.deadlock.FinalStateDetected;
import plug.verifiers.predicates.PredicateVerifier;
import plug.verifiers.predicates.PredicateViolationEvent;
import properties.PropositionalLogic.PropositionalLogicModel.Expression;
import properties.PropositionalLogic.interpreter.Evaluator;
import properties.PropositionalLogic.parser.Parser;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertThat;

//import plug.verifiers.buchi.AcceptanceCycleDetectedEvent;
//import plug.verifiers.buchi.QueueBuchiVerifier;
//import plug.verifiers.buchi.SimpleBuchiRuntime;

/**
 * @author Ciprian Teodorov (ciprian.teodorov@ensta-bretagne.fr)
 *         Created on 10/03/16.
 */
public class ExplicitRuntimeTest {

    protected ILanguagePlugin module = new ExplicitPlugin();

    protected ITransitionRelation load(String fileName) throws Exception {
        return module.getLoader().getRuntime(fileName);
    }

    protected IStateSpaceManager createStateSpaceManager() {
        IStateSpaceManager stateSpaceManager = new SimpleStateSpaceManager<>();
        stateSpaceManager.fullConfigurationStorage();
        stateSpaceManager.countingTransitionStorage();
        return stateSpaceManager;
    }

    public AbstractExplorer assertBFS(String filename, int expectedNumberOfStates) throws Exception {
        return assertBFS(filename, expectedNumberOfStates, -1);
    }

    public AbstractExplorer assertBFS(String filename, int expectedNumberOfStates, int expectedNumberOfTransitions) throws Exception {
        ITransitionRelation runtime = load(filename);
        AbstractExplorer explorer = new BFSExplorer(runtime, createStateSpaceManager());
        explorer.execute();

        Assert.assertEquals(expectedNumberOfStates, explorer.getStateSpaceManager().size());
        if (expectedNumberOfTransitions >= 0) {
            Assert.assertEquals(expectedNumberOfTransitions, explorer.getStateSpaceManager().transitionCount());
        }
        return explorer;
    }

    public boolean deadlockfree(String filename, int expectedStateCount, int expectedTransitionCount) throws Exception {
        ITransitionRelation runtime = load(filename);
        AbstractExplorer explorer = new BFSExplorer(runtime, createStateSpaceManager());

        DeadlockVerifier dV = new DeadlockVerifier(explorer.getAnnouncer());

        boolean deadLockFree[] = new boolean[] { true };
        dV.announcer.when(FinalStateDetected.class, (ann, ev) -> {
            System.out.println("Final state detected: " + ev.getFinalState() );
            deadLockFree[0] = false;
        });

        explorer.execute();

        Assert.assertEquals(expectedStateCount, explorer.getStateSpaceManager().size());
        if (expectedTransitionCount >= 0) {
            Assert.assertEquals(expectedTransitionCount, explorer.getStateSpaceManager().transitionCount());
        }
        return deadLockFree[0];
    }

    public boolean verify(String filename, String predicate, int expectedStateCount, int expectedTransitionCount) throws Exception {
        ITransitionRelation runtime = load(filename);
        AbstractExplorer explorer = new BFSExplorer(runtime, createStateSpaceManager());

        PredicateVerifier<ExplicitConfiguration> pV = new PredicateVerifier<>(explorer.getAnnouncer());

        //create the predicate
        Expression exp = Parser.parse(predicate);
        Evaluator evaluator = new Evaluator();
        AtomEvaluator atomE = new AtomEvaluator();
        atomE.program = ((ExplicitTransitionRelation)runtime).program;
        evaluator.addAtomEvaluator("exp", atomE);
        evaluator.setDefaultEvaluator(atomE);
        Predicate ipred = new Predicate(exp, evaluator, atomE);

        pV.predicates.add(ipred);

        boolean ret[] = new boolean[] {true};
        pV.announcer.when(PredicateViolationEvent.class, (announcer, event) -> {
            System.err.println("predicate violated: " + event.getViolated());
            explorer.getMonitor().hasToFinish();
            ret[0] = false;
        });

        explorer.execute();

        Assert.assertEquals(expectedStateCount, explorer.getStateSpaceManager().size());
        Assert.assertEquals(expectedTransitionCount, explorer.getStateSpaceManager().transitionCount());

        return ret[0];
    }

    @Test
    public void testDifferentRuntimeViews() {
        IRuntimeView view1 = module.getRuntimeView(new ExplicitTransitionRelation("view1", null));
        IRuntimeView view2 = module.getRuntimeView(new ExplicitTransitionRelation("view2", null));
        assertNotSame(view1, view2);
    }

    @Test
    public void testLoad() throws Exception {
        ExplicitTransitionRelation runtime = (ExplicitTransitionRelation) load("tests/resources/simple.json");
        assertThat(runtime.program, is(notNullValue()));
        assertThat(runtime.program.variables, is(notNullValue()));
        assertThat(runtime.program.variables.size(), is(3));
        assertThat(runtime.program.variables.get("b"), is(1));
        assertThat(runtime.program.initial, is(new int[]{0}));
        assertThat(runtime.program.states.length, is(3));
        assertThat(runtime.program.fanout[0].length, is(2));
        assertThat(runtime.program.fanout[1].length, is(2));
    }

    @Test
    public void testSimple() throws Exception {
        assertBFS("tests/resources/simple.json",3, 4);
    }

    @Test
    public void testAlice_Bob() throws Exception {
        assertBFS("tests/resources/alice-bob.json", 11, 17);
    }

    @Test
    public void testPredicateAlice_Bob() throws Exception {
        assertThat(verify("tests/resources/alice-bob.json", "!(|aliceState == 2| && |bobState == 3|)", 11, 17), is(true));
    }

//    @Test
//    public void testFairnessAliceBob() {
//        //"!(G(p0 -> F q0) && G(p1 -> F q1))"
//        SimpleBuchiRuntime br = new SimpleBuchiRuntime();
//        br.initialState = 0;
//        br.accepting = new boolean[] {false /*i:0*/, true/*a:1*/, true/*b:2*/};
//        br.buchi = new LinkedHashMap<>();
//
//        Function<CompositeConfiguration, Integer> aliceState = (c) -> ((ExplicitConfiguration)c.get(0)).values[0];
//        Function<CompositeConfiguration, Integer> aliceFlag = (c) -> ((ExplicitConfiguration)c.get(0)).values[1];
//        int aliceCritical = 2;
//
//        Function<CompositeConfiguration, Integer> bobState = (c) -> ((ExplicitConfiguration)c.get(0)).values[2];
//        Function<CompositeConfiguration, Integer> bobFlag = (c) -> ((ExplicitConfiguration)c.get(0)).values[3];
//        int bobCritical = 3;
//
//        BiPredicate<Short, CompositeConfiguration> pi2i = (state, c) -> state == 0;
//        BiPredicate<Short, CompositeConfiguration> pi2a = (state, c) -> state == 0 && aliceFlag.apply(c) == 1 && (aliceState.apply(c) != aliceCritical); //p & ! q
//        BiPredicate<Short, CompositeConfiguration> pa2a = (state, c) -> state == 1 && !(aliceState.apply(c) == aliceCritical);
//        BiPredicate<Short, CompositeConfiguration> pi2b = (state, c) -> state == 0 && bobFlag.apply(c) == 1 && (bobState.apply(c) != bobCritical); //p & ! q
//        BiPredicate<Short, CompositeConfiguration> pb2b = (state, c) -> state == 2 && !(bobState.apply(c) == bobCritical);
//
//        br.buchi.put(pi2i, (short)0);
//        br.buchi.put(pi2a, (short)1);
//        br.buchi.put(pa2a, (short)1);
//        br.buchi.put(pi2b, (short)2);
//        br.buchi.put(pb2b, (short)2);
//
//        BFSExplorer controller = new BFSExplorer();
//        controller.stateSpaceManager = new SimpleStateSpaceManager();
//        controller.stateSpaceManager.fullTransitionStorage();
//
//
//        ITransitionRelation runtime = load("tests/resources/alice-bob.json");
//        controller.addRuntime(runtime);
//
//
//        QueueBuchiVerifier bV = new QueueBuchiVerifier(controller.getAnnouncer(), br);
//
//        boolean acceptanceCycle[] = new boolean[] {false};
//        bV.announcer.when(AcceptanceCycleDetectedEvent.class, (ann, evt) -> {
//            acceptanceCycle[0] = true;
//        });
//
//        BiConsumer<Announcer, ExecutionEndedEvent> sizeChecker;
//        controller.announcer.when(ExecutionEndedEvent.class, sizeChecker = (a, e) -> assertEquals(33, e.getSource().getStateSpaceManager().size()));
//
//        controller.explore();
//        assertTrue(acceptanceCycle[0]);
//
//        //"!((p0 -> F q0) && (p1 -> F q1))" -- weak property -- there is a path where this is true
//        br.accepting[0] = true;
//        br.buchi.remove(pi2i);
//        acceptanceCycle[0] = false;
//
//        controller.stateSpaceManager = new SimpleStateSpaceManager();
//        controller.stateSpaceManager.fullTransitionStorage();
//        controller.getAnnouncer().remove(sizeChecker);
//        controller.announcer.when(ExecutionEndedEvent.class, (a, e) -> assertEquals(23, e.getSource().getStateSpaceManager().size()));
//
//        controller.explore();
//        assertFalse(acceptanceCycle[0]);
//        //StateSpace2TGF.toTGF(controller.getStateSpaceManager().getGraphView(), true, "alice_bob_fairness.tgf");
//    }

    @Test
    public void testDeadlockfreeSimple() throws Exception {
        assertThat(deadlockfree("tests/resources/simple.json", 3, 4), is(false));
    }

    @Test
    public void testDeadlockfreeAlice_Bob() throws Exception {
        assertThat(deadlockfree("tests/resources/alice-bob.json", 11, 17), is(true));
    }


}
