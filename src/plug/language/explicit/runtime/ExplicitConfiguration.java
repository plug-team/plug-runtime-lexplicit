package plug.language.explicit.runtime;

import java.util.Arrays;
import plug.core.defaults.DefaultConfiguration;

public class ExplicitConfiguration extends DefaultConfiguration<ExplicitConfiguration> {
	public int id; //the state id is here only to simplify the getFireableTransitions
	public int[] values;
	
	@Override
	public ExplicitConfiguration createCopy() {
		ExplicitConfiguration newC = new ExplicitConfiguration();
		newC.id = id;
		newC.values = Arrays.copyOf(values, values.length);
		return newC;
	}
	
	@Override
	public int hashCode() {
		return values.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj instanceof ExplicitConfiguration) {
			ExplicitConfiguration other = (ExplicitConfiguration) obj;
			return Arrays.equals(values, other.values);
		}
		return false;
	}

	@Override
	public String toString() {
		return Arrays.toString(values);
	}
}
