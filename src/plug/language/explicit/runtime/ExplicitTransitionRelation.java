package plug.language.explicit.runtime;

import java.util.*;
import java.util.stream.Collectors;

import plug.core.IAtomicPropositionsEvaluator;
import plug.core.IAtomicPropositionsProvider;
import plug.core.IConcurrentTransitionRelation;
import plug.core.IFiredTransition;
import plug.language.explicit.model.ExplicitProgram;
import plug.statespace.transitions.FiredTransition;

public class ExplicitTransitionRelation implements IConcurrentTransitionRelation<ExplicitTransitionRelation, ExplicitConfiguration, Integer> {

	public final String name;

	public final ExplicitProgram program;

	public ExplicitTransitionRelation(String name, ExplicitProgram program) {
		this.name = name;
		this.program = program;
	}

	@Override
	public ExplicitTransitionRelation createCopy() {
		return new ExplicitTransitionRelation(name, program);
	}
	
	@Override
	public Set<ExplicitConfiguration> initialConfigurations() {
		Set<ExplicitConfiguration> initialSet = new HashSet<>();
		for (int i : program.initial) {
			ExplicitConfiguration config = new ExplicitConfiguration();
			config.id = i;
			config.values = program.states[i];
			initialSet.add(config);
		}

		return initialSet;
	}

	@Override
	public Collection<Integer> fireableTransitionsFrom(ExplicitConfiguration source) {
		int[] fanout = program.getFireableTransitions(source.id);

		return Arrays.stream(fanout).boxed().collect(Collectors.toList());
	}

	@Override
	public IFiredTransition<ExplicitConfiguration, ?> fireOneTransition(ExplicitConfiguration source, Integer transition) {
		//create the new configuration
		ExplicitConfiguration target = new ExplicitConfiguration();
		target.id 	  = transition;
		target.values = program.states[transition];

		return new FiredTransition<>(source, target, transition);
	}

	IAtomicPropositionsEvaluator<ExplicitConfiguration, Integer> evaluator = new IAtomicPropositionsEvaluator<ExplicitConfiguration, Integer>() {
		EqualityCondition atomicPropositionASTs[];
		@Override
		public int[] registerAtomicPropositions(String[] atomicPropositions) throws Exception {
			atomicPropositionASTs = new EqualityCondition[atomicPropositions.length];
			for (int i = 0; i < atomicPropositions.length; i++) {
				atomicPropositionASTs[i] = parse(atomicPropositions[i]);
			}
			return new int[0];
		}

		@Override
		public boolean[] getAtomicPropositionValuations(ExplicitConfiguration configuration) {
			boolean valuation[] = new boolean[atomicPropositionASTs.length];
			for (int i = 0; i < atomicPropositionASTs.length; i++) {
				valuation[i] = configuration.values[program.variables.get(atomicPropositionASTs[i].variable)] == atomicPropositionASTs[i].value;
			}
			return valuation;
		}

		public EqualityCondition parse(String code) {
			EqualityCondition condition = new EqualityCondition();
			StringTokenizer tokenizer = new StringTokenizer(code);

			condition.variable = tokenizer.nextToken();
			/*String operator =*/ tokenizer.nextToken();
			condition.value = Integer.parseInt(tokenizer.nextToken());
			return condition;
		}

		class EqualityCondition {
			String variable;
			int value;
		}
	};


	@Override
	public IAtomicPropositionsEvaluator getAtomicPropositionEvaluator() {
		return evaluator;
	}
}
