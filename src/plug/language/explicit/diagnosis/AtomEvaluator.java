package plug.language.explicit.diagnosis;

import plug.language.explicit.runtime.ExplicitConfiguration;
import plug.language.explicit.model.ExplicitProgram;
import properties.PropositionalLogic.interpreter.IAtomEvaluator;

import java.util.StringTokenizer;

/**
 * @author Ciprian Teodorov (ciprian.teodorov@ensta-bretagne.fr)
 *         Created on 11/03/16.
 */
public class AtomEvaluator implements IAtomEvaluator {
    ExplicitConfiguration configuration;
    public ExplicitProgram program;
    @Override
    public Object parse(String code) {
        EqualityCondition condition = new EqualityCondition();
        StringTokenizer tokenizer = new StringTokenizer(code);

        condition.variable = tokenizer.nextToken();
        /*String operator =*/ tokenizer.nextToken();
        condition.value = Integer.parseInt(tokenizer.nextToken());
        return condition;
    }

    @Override
    public boolean evaluate(Object atomicBooleanExpression) {
        EqualityCondition condition = (EqualityCondition)atomicBooleanExpression;

        return configuration.values[program.variables.get(condition.variable)] == condition.value;
    }
}

class EqualityCondition {
    String variable;
    int value;
}