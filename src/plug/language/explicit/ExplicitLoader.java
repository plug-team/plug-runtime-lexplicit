package plug.language.explicit;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import plug.core.ILanguageLoader;
import plug.language.explicit.model.ExplicitProgram;
import plug.language.explicit.runtime.ExplicitTransitionRelation;

/**
 * @author Ciprian Teodorov (ciprian.teodorov@ensta-bretagne.fr)
 *         Created on 10/03/16.
 */
public class ExplicitLoader implements ILanguageLoader<ExplicitTransitionRelation> {

    public ExplicitTransitionRelation _loadRuntime(File modelFile) throws IOException {
        return new ExplicitTransitionRelation(modelFile.getName(), loadModel(modelFile));
    }
    
    public ExplicitProgram loadModel(File modelFile) throws IOException {
    	 ObjectMapper mapper = new ObjectMapper();
         mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
         return mapper.readValue(modelFile, ExplicitProgram.class);
    }

    @Override
    public ExplicitTransitionRelation getRuntime(URI modelURI, Map<String, Object> options) throws IOException {
        return _loadRuntime(new File(modelURI));
    }
}
