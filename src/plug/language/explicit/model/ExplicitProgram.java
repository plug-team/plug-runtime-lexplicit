package plug.language.explicit.model;

import java.util.Map;

/**
 * @author Ciprian Teodorov (ciprian.teodorov@ensta-bretagne.fr)
 *         Created on 10/03/16.
 */
public class ExplicitProgram {
	public Map<String, Integer> variables;
	public int initial[];
	public int states[][];
	public int fanout[][]; //an entry in the array for fanout of each state

	public int[] getFireableTransitions(int stateID) {
		return fanout[stateID];
	}
}
