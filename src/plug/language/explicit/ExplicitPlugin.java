package plug.language.explicit;

import plug.core.ILanguagePlugin;
import plug.core.IRuntimeView;
import plug.language.explicit.runtime.ExplicitTransitionRelation;

/**
 * Created by Ciprian TEODOROV on 03/03/17.
 */
public class ExplicitPlugin implements ILanguagePlugin<ExplicitTransitionRelation> {
    ExplicitLoader loader = new ExplicitLoader();

    @Override
    public String[] getExtensions() {
        return new String[]{".json"};
    }

    @Override
    public String getName() {
        return "Explicit";
    }

    @Override
    public ExplicitLoader getLoader() {
        return loader;
    }

    @Override
    public IRuntimeView getRuntimeView(ExplicitTransitionRelation runtime) {
        return new ExplicitRuntimeView(runtime);
    }
}
