package plug.language.explicit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import plug.core.IRuntimeView;
import plug.core.view.ConfigurationItem;
import plug.language.explicit.runtime.ExplicitConfiguration;
import plug.language.explicit.runtime.ExplicitTransitionRelation;

/**
 * Created by Ciprian TEODOROV on 03/03/17.
 */
public class ExplicitRuntimeView implements IRuntimeView<ExplicitConfiguration, Integer> {
    private final ExplicitTransitionRelation runtime;

    public ExplicitRuntimeView(ExplicitTransitionRelation runtime) {
        this.runtime = runtime;
    }

    @Override
    public ExplicitTransitionRelation getRuntime() {
        return runtime;
    }

    @Override
    public List<ConfigurationItem> getConfigurationItems(ExplicitConfiguration value) {
        List<ConfigurationItem> variableItems = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : runtime.program.variables.entrySet()) {
            String name = entry.getKey() + " = " + value.values[entry.getValue()];
            variableItems.add(new ConfigurationItem("variable", name, null, null));
        }
        return variableItems;
    }

    @Override
    public String getFireableTransitionDescription(Integer transition) {
        return transition.toString();
    }
}
